<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2016.1.112/styles/kendo.common.min.css" />
        <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2016.1.112/styles/kendo.metro.min.css" />
        <style>
            .k-grid  .k-grid-header  .k-header  .k-link {
                height: auto;
            }

            .k-grid  .k-grid-header  .k-header {
                white-space: normal;
            }
            .k-grid-delete{
                margin: 0px !important;
                min-width: 0px !important;
            }
            .k-grid-delete  .k-delete{
                margin: 0px !important;
            }
        </style>
    </head>
    <body>
        <div id="grid"></div>
        <script src="https://api.bitrix24.com/api/v1/"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.12.0/moment-with-locales.min.js"></script>
        <script src="https://kendo.cdn.telerik.com/2016.1.112/js/jquery.min.js"></script>
        <script src="https://kendo.cdn.telerik.com/2016.1.112/js/kendo.all.min.js"></script>
        <script src="https://kendo.cdn.telerik.com/2016.1.112/js/messages/kendo.messages.ru-RU.min.js"></script>
        <script src="https://kendo.cdn.telerik.com/2016.1.112/js/cultures/kendo.culture.ru-RU.min.js" type="text/javascript"></script>
        <script src="https://kendo.cdn.telerik.com/2016.1.112/js/jszip.min.js"></script>
        <!-- Load Pako ZLIB library to enable PDF compression -->
        <script src="https://kendo.cdn.telerik.com/2016.1.112/js/pako_deflate.min.js"></script>
        <script src="resources/funcs.js" type="text/javascript"></script>
        <script type="text/javascript">
            moment.locale('ru');
            kendo.culture("ru-RU");
            var data = [];
            var tkp;
            function getData() {
                return data;
            }
            function getFolder(cb) {

                BX24.callMethod("disk.storage.getlist",
                        {
                            filter: {
                                'ENTITY_TYPE': 'common'
                            }
                        },
                function (result)
                {
                    var storageID = result.data()[0].ID;
                    BX24.callMethod("disk.storage.getchildren",
                            {
                                id: storageID,
                                filter: {
                                    CREATED_BY: 1
                                }
                            },
                    function (result)
                    {
                        var exist = false;
                        var folderID;
                        $.each(result.data(), function (index, item) {
                            if (item.NAME === "ТКП") {
                                exist = true;
                                folderID = item.ID;
                            }
                        });
                        if (!exist) {
                            BX24.callMethod("disk.storage.addfolder",
                                    {
                                        id: storageID,
                                        data: {'NAME': 'ТКП'}
                                    },
                            function (result)
                            {
                                folderID = result.data().ID;
                                if (cb) {
                                    cb(folderID);
                                }
                            });
                        } else {
                            if (cb) {
                                cb(folderID);
                            }
                        }
                    });
                });
            }
            function getParameterByName(name) {
                var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
                return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
            }
            $(document).ready(function () {
                $("body").on("click", ".k-grid-save-changes", function (e) {
                    var dataToSave = JSON.stringify(getData());
                    var fields = {};
                    fields[tkp] = dataToSave;
                    BX24.callMethod("crm.deal.update",
                            {
                                id: getParameterByName("id"),
                                fields: fields,
                                params: {"REGISTER_SONET_EVENT": "Y"}
                            },
                    function (result)
                    {
                        getFolder(function (folderId) {
                            getSubFolder(folderId, function (subFolderId) {
                                kendo.drawing.drawDOM($("#calendar"), {
                                    paperSize: "A4",
                                    landscape: true,
                                    margin: "3mm"
                                }).then(function (root) {
                                    return draw.exportPDF(root, {
                                        paperSize: "A4",
                                        landscape: true,
                                        margin: "3mm"
                                    });
                                }).done(function (data) {
                                    BX24.callMethod("disk.folder.uploadfile",
                                            {
                                                id: subFolderId,
                                                data: {
                                                    NAME: getParameterByName("name") + "-" + moment().format("YYYY-MM-DD") + ".pdf"
                                                },
                                                fileContent: data
                                            },
                                    function (result)
                                    {
                                        BX24.reloadWindow();
                                    });
                                });
                            });
                        });
                    });
                });
                if (getParameterByName("id")) {

                    BX24.callMethod('entity.item.get', {ENTITY: "Fields"}, function (res) {
                        if (res.data() && res.data().length > 0)
                        {
                            var item = res.data()[0];
                            tkp = item.PROPERTY_VALUES.tkp;

                            BX24.callMethod("crm.deal.get", {id: getParameterByName("id")},
                            function (result)
                            {
                                if (result.data()[tkp]) {
                                    data = JSON.parse(result.data()[tkp]);
                                    $("#grid").data("kendoGrid").dataSource.fetch();
                                }
                            });
                        }
                    });
                }
            });
            BX24.init(function () {
                $("#grid").kendoGrid({
                    dataSource: {
                        transport: {
                            read: function (options) {
                                options.success(getData());
                            },
                            update: function (options) {
                                $.each(getData(), function (index, item) {
                                    if (item.id === options.data.id) {
                                        options.data.taxsum = taxsumfunc(options.data);
                                        options.data.supsum = supsumfunc(options.data);
                                        options.data.logisticsum = logisticfunc(options.data);
                                        options.data.margsum = margsumfunc(options.data);
                                        options.data.profitsum = profitsumfunc(options.data);
                                        getData()[index] = options.data;
                                    }
                                });
                                options.success(options.data);
                            },
                            destroy: function (options) {
                                $.each(getData(), function (index, item) {
                                    if (item.id === options.data.id) {
                                        getData().splice(index, 1);
                                    }
                                });
                                options.success(options.data);
                            },
                            create: function (options) {
                                options.data.id = getData().length + 1;
                                options.data.taxsum = taxsumfunc(options.data);
                                options.data.supsum = supsumfunc(options.data);
                                options.data.logisticsum = logisticfunc(options.data);
                                options.data.margsum = margsumfunc(options.data);
                                options.data.profitsum = profitsumfunc(options.data);
                                getData().push(options.data);
                                options.success(options.data);
                            }
                        },
                        pageSize: 20,
                        sort: {field: "id", dir: "asc"},
                        autoSync: true,
                        schema: {
                            model: {
                                id: "id",
                                fields: {
                                    id: {editable: false, nullable: true},
                                    name: {validation: {required: true}},
                                    unit: {defaultValue: {id: 0, name: "шт."}},
                                    qty: {type: "number", validation: {required: true, min: 1}},
                                    taxsum: {type: "number", editable: false, validation: {required: true, min: 0}},
                                    time: {type: "number", validation: {required: true, min: 0}, nullable: true},
                                    arm: {type: "number", validation: {required: true, min: 0}, nullable: true},
                                    kof: {type: "number", validation: {required: true, min: 0}, nullable: true},
                                    extra: {type: "number", validation: {required: true, min: 0}, nullable: true},
                                    supsum: {type: "number", editable: false, validation: {required: true, min: 0}},
                                    supplier: {validation: {required: true}},
                                    logistic: {type: "number", validation: {required: true, min: 0}, nullable: true},
                                    logisticsum: {type: "number", editable: false, validation: {required: true, min: 0}},
                                    marg: {type: "number", defaultValue: 12, validation: {required: true, min: 0}},
                                    margsum: {type: "number", editable: false, validation: {required: true, min: 0}},
                                    profitsum: {type: "number", editable: false, validation: {required: true, min: 0}}
                                }
                            }
                        },
                        aggregate: [
                            {field: "taxsum", aggregate: "sum"},
                            {field: "supsum", aggregate: "sum"},
                            {field: "logistic", aggregate: "sum"},
                            {field: "logisticsum", aggregate: "sum"},
                            {field: "marg", aggregate: "average"},
                            {field: "margsum", aggregate: "sum"},
                            {field: "profitsum", aggregate: "sum"}
                        ]
                    },
                    height: 600,
                    groupable: false,
                    sortable: true,
                    resizable: true,
                    pageable: false,
                    filterable: false,
                    editable: true,
                    toolbar: [{name: "create"}, {name: "save", text: "Сохранить расчет"}],
                    columns: [
                        {
                            field: "id",
                            title: "№",
                            width: 64,
                            locked: true,
                            lockable: false
                        },
                        {
                            field: "name",
                            title: "Наименование",
                            width: 300,
                            locked: true,
                            lockable: false
                        },
                        {
                            command: [{name: "destroy", text: " "}],
                            title: " ",
                            width: 52,
                            locked: true,
                            lockable: false
                        },
                        {
                            field: "unit",
                            title: "Ед. изм.",
                            width: 98,
                            editor: categoryDropDownEditor,
                            template: "#=unit.name#"
                        },
                        {
                            field: "qty",
                            width: 91,
                            title: "Кол-во"
                        },
                        {
                            field: "taxsum",
                            title: "Цена с НДС",
                            width: 127,
                            template: "<strong>#=kendo.toString(taxpricefunc(data), 'C')#</strong>"
                        },
                        {
                            field: "taxsum",
                            title: "Сумма с НДС",
                            width: 150,
                            aggregates: ["sum"],
                            footerTemplate: "<div>#=kendo.toString(sum, 'C')#</div>",
                            template: "<strong>#=kendo.toString(taxsumfunc(data), 'C')#</strong>"
                        },
                        {
                            field: "time",
                            title: "Срок поставки",
                            width: 130
                        },
                        {
                            field: "arm",
                            title: "Арматура с НДС",
                            width: 130,
                            format: "{0:c}",
                            attributes: {
                                style: "background-color: \\#EBF1DE"
                            }
                        },
                        {
                            field: "kof",
                            title: "КОФ с НДС",
                            width: 130,
                            format: "{0:c}",
                            attributes: {
                                style: "background-color: \\#EBF1DE"
                            }
                        },
                        {
                            field: "extra",
                            title: "Доп. оборуд.",
                            width: 130,
                            format: "{0:c}",
                            attributes: {
                                style: "background-color: \\#EBF1DE"
                            }
                        },
                        {
                            field: "supsum",
                            title: "Цена закупа с НДС",
                            width: 130,
                            attributes: {
                                style: "background-color: \\#FDEADA"
                            },
                            template: "<strong>#=kendo.toString(suppricefunc(data), 'C')#</strong>"
                        },
                        {
                            field: "supsum",
                            title: "Сумма закупа",
                            width: 130,
                            aggregates: ["sum"],
                            footerTemplate: "<div>#=kendo.toString(sum, 'C')#</div>",
                            attributes: {
                                style: "background-color: \\#FDEADA"
                            },
                            template: "<strong>#=kendo.toString(supsumfunc(data), 'C')#</strong>"
                        },
                        {
                            field: "supplier",
                            title: "Поставщик",
                            width: 130,
                            attributes: {
                                style: "background-color: \\#EBF1DE"
                            }
                        },
                        {
                            field: "logistic",
                            title: "Общие транс. расх.",
                            width: 130,
                            format: "{0:c}",
                            aggregates: ["sum"],
                            footerTemplate: "<div>#=kendo.toString(sum, 'C')#</div>",
                            attributes: {
                                style: "background-color: \\#EBF1DE"
                            }
                        },
                        {
                            field: "logisticsum",
                            title: "Сумма транс. расх.",
                            width: 130,
                            aggregates: ["sum"],
                            footerTemplate: "<div>#=kendo.toString(sum, 'C')#</div>",
                            attributes: {
                                style: "background-color: \\#FDEADA"
                            },
                            template: "<strong>#=kendo.toString(logisticfunc(data), 'C')#</strong>"
                        },
                        {
                            field: "marg",
                            title: "Рентаб. в %",
                            width: 130,
                            aggregates: ["average"],
                            footerTemplate: "<div>#=average#%</div>",
                            attributes: {
                                style: "background-color: \\#EBF1DE"
                            }
                        },
                        {
                            field: "margsum",
                            title: "Сумма рентаб. в руб.",
                            width: 130,
                            aggregates: ["sum"],
                            footerTemplate: "<div>#=kendo.toString(sum, 'C')#</div>",
                            attributes: {
                                style: "background-color: \\#FDEADA"
                            },
                            template: "<strong>#=kendo.toString(margsumfunc(data), 'C')#</strong>"
                        },
                        {
                            field: "profitsum",
                            title: "Премия Снабжения в руб.",
                            width: 130,
                            aggregates: ["sum"],
                            footerTemplate: "<div>#=kendo.toString(sum, 'C')#</div>",
                            attributes: {
                                style: "background-color: \\#FDEADA"
                            },
                            template: "<strong>#=kendo.toString(profitsumfunc(data), 'C')#</strong>"
                        }
                    ]
                });
                BX24.fitWindow();
                BX24.setTitle($("title").text());
            });
            function categoryDropDownEditor(container, options) {
                $('<input required data-text-field="name" data-value-field="id" data-bind="value:' + options.field + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            autoBind: false,
                            dataSource: {
                                data: [{id: "0", name: "шт."}, {id: "1", name: "комп."}]
                            }
                        });
            }
        </script>
    </body>
</html>
