<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2016.1.112/styles/kendo.common-office365.min.css"/>
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2016.1.112/styles/kendo.office365.min.css"/>
</head>
<body>
<div id="grid"></div>
<a id="configLink" class="k-button hidden" href="config.php" style="margin-top: 10px;"><i class="fa fa-cog"></i>
    Настройки</a>
<script src="https://api.bitrix24.com/api/v1/"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.12.0/moment-with-locales.min.js"></script>
<script src="https://kendo.cdn.telerik.com/2016.1.112/js/jquery.min.js"></script>
<script src="https://kendo.cdn.telerik.com/2016.1.112/js/kendo.all.min.js"></script>
<script src="https://kendo.cdn.telerik.com/2016.1.112/js/messages/kendo.messages.ru-RU.min.js"></script>
<script src="https://kendo.cdn.telerik.com/2016.1.112/js/jszip.min.js"></script>
<!-- Load Pako ZLIB library to enable PDF compression -->
<script src="https://kendo.cdn.telerik.com/2016.1.112/js/pako_deflate.min.js"></script>
<script src="resources/funcs.js"></script>
<script type="text/javascript">
    moment.locale('ru');
    function convertOp(op) {
        switch (op) {
            case "lt":
                return "<";
            case  "lte":
                return "<=";
            case  "gt":
                return ">";
            case  "gte":
                return ">=";
            case "eq":
                return "";
            case  "neq":
                return "!";
            case  "startswith":
                return "%";
            case  "endswith":
                return "%";
            case  "contains":
                return "%";
        }
    }

    function parseFilters(filter) {
        var result = {};
        $.each(filter.filters, function (index, item) {
            result[convertOp(item.operator) + item.field] = item.value;
        });
        return result;
    }

    $(document).ready(function () {

    });
    BX24.init(function () {
        getFields(function (fields) {
            var columns = [
                {
                    field: "ID",
                    title: "№",
                    filterable: false
                }, {
                    field: "TITLE",
                    title: "Название",
                    template: '<a href="https://' + BX24.getDomain() + '/crm/deal/show/#=ID#/" target="_blank">' +
                    "#=TITLE#</a>"
                },
                {
                    command: [{
                        text: "Расчет",
                        click: function (e) {
                            // e.target is the DOM element representing the button
                            var tr = $(e.target).closest("tr"); // get the current table row (tr)
                            // get the data bound to the current table row
                            var data = this.dataItem(tr);
                            window.open("https://bitrix24.azurewebsites.net/quotes/calc.php?id=" + data.ID + "&name=" + data.TITLE, "_self");
                        }
                    }]
                }
            ];
            $.each(fields, function (index, item) {
                if (item !== "ID" && item !== "TITLE") {
                    columns.push({
                        command: [{
                            text: "Файлы",
                            click: function (e) {
                                // e.target is the DOM element representing the button
                                var tr = $(e.target).closest("tr"); // get the current table row (tr)
                                // get the data bound to the current table row
                                var data = this.dataItem(tr);
                                $("<div />").kendoWindow({
                                    title: "Предпросмотр",
                                    resizable: true,
                                    width: 600,
                                    height: 500,
                                    modal: true,
                                    content: "https://drive.google.com/viewerng/viewer?embedded=true&url=" +
                                    encodeURIComponent("https://" + BX24.getDomain() +
                                            (data[item].length && data[item].length > 0 ? data[item][0] : data[item]).downloadUrl),
                                    actions: ["close"],
                                    open: function (e) {
                                        this.wrapper.css({top: 10});
                                    }
                                }).data("kendoWindow").center().open();
                            }
                        }]
                    });
                }
            });
            $("#grid").kendoGrid({
                dataSource: {
                    type: "json",
                    transport: {
                        read: {
                            url: "https://" + BX24.getAuth().domain + "/rest/crm.deal.list.json"
                        },
                        parameterMap: function (data, type) {
                            if (type === "read") {
                                return {
                                    auth: BX24.getAuth().access_token,
                                    start: data.skip,
                                    select: fields,
                                    filter: parseFilters(data.filter)
                                };
                            }
                        }
                    },
                    schema: {
                        total: "total",
                        data: "result",
                        errors: "error",
                        model: {id: "ID"},
                        parse: function (response) {
                            return response;
                        }
                    },
                    pageSize: 25,
                    serverPaging: true,
                    serverFiltering: true,
                    filter: [
                        {field: "CLOSED", operator: "eq", value: "N"}
                    ]
                },
                height: 1500,
                groupable: false,
                sortable: false,
                pageable: true,
                filterable: true,
                selectable: true,
                columns: columns
            });
            BX24.fitWindow();
        });
        if (BX24.isAdmin()) {
            $("#configLink").removeClass("hidden");
        }
        BX24.setTitle($("title").text());
    });
</script>
</body>
</html>
