<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <style>
            /* Space out content a bit */
            body {
                color:#535c69;
                padding-top: 20px;
                padding-bottom: 20px;
            }

            /* Everything but the jumbotron gets side spacing for mobile first views */
            .header,
            .marketing,
            .footer {
                padding-right: 15px;
                padding-left: 15px;
            }

            /* Custom page header */
            .header {
                border-bottom: 1px solid #e5e5e5;
            }
            /* Make the masthead heading the same height as the navigation */
            .header h3 {
                padding-bottom: 19px;
                margin-top: 0;
                margin-bottom: 0;
                line-height: 40px;
            }

            /* Custom page footer */
            .footer {
                padding-top: 19px;
                color: #777;
                border-top: 1px solid #e5e5e5;
            }

            /* Customize container */
            @media (min-width: 768px) {
                .container {
                    max-width: 730px;
                }
            }
            .container-narrow > hr {
                margin: 30px 0;
            }

            /* Main marketing message and sign up button */
            .jumbotron {
                text-align: center;
                border-bottom: 1px solid #e5e5e5;
            }
            .jumbotron .btn {
                padding: 14px 24px;
                font-size: 21px;
            }

            /* Supporting marketing content */
            .marketing {
                margin: 40px 0;
            }
            .marketing p + h4 {
                margin-top: 28px;
            }

            /* Responsive: Portrait tablets and up */
            @media screen and (min-width: 768px) {
                /* Remove the padding we set earlier */
                .header,
                .marketing,
                .footer {
                    padding-right: 0;
                    padding-left: 0;
                }
                /* Space out the masthead */
                .header {
                    margin-bottom: 30px;
                }
                /* Remove the bottom border on the jumbotron for visual effect */
                .jumbotron {
                    border-bottom: 0;
                }
            }
            .goBack{
                position: absolute;
                top: 10px;
                left: 10px;
                font-size: 2em;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <a href="#" id="goBack" class="goBack"><i class="fa fa-arrow-circle-left"></i>Назад</a>
            <div class="content">
                <div class="jumbotron inner-content">
                    <div class="form-horizontal" style="text-align: initial;">
                        <div class="form-group">
                            <label class="col-sm-5 control-label" for="tkp">Поле для хранения данных расчета</label>
                            <div class="col-sm-7">
                                <select class="form-control" data-placeholder="Выберите поле..." id="tkp"><option></option></select>
                            </div>
                        </div>
                    </div>
                    <a id="save" class="btn btn-lg btn-success" role="button" data-loading-text="Сохранение...">Сохранить</a>
                </div>
            </div>
        </div>
        <script src="https://api.bitrix24.com/api/v1/"></script>
        <script src="https://code.jquery.com/jquery-1.12.2.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.12.0/moment-with-locales.min.js"></script>
        <script type="text/javascript">
            function getParameterByName(name) {
                var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
                return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
            }
            function getNamebyID(index, item) {
                if (index.indexOf('UF_CRM') === 0) {
                    return item.formLabel;
                } else {
                    switch (index) {
                        case "ADDRESS":
                            return "Фактический адрес";
                        case "ADDRESS_LEGAL":
                            return "Юридический адрес";
                        case "BANKING_DETAILS":
                            return "Банковские реквизиты";
                        case "COMMENTS":
                            return "Комментарий";
                        case "REVENUE":
                            return "Годовой оборот";
                        case "TITLE":
                            return "Название";
                    }
                }
            }
            BX24.init(function () {
                BX24.callMethod('crm.deal.fields', {full: true}, function (res) {
                    if (res.data())
                    {
                        for (var prop in res.data()) {
                            if (!res.data().hasOwnProperty(prop)) {
                                //The current property is not a direct property of p
                                continue;
                            }
                            var item = res.data()[prop];
                            if (item.type === "string" && getNamebyID(prop, item)) {
                                $("#tkp").append($('<option value=' + prop + '>' + getNamebyID(prop, item) + '</option>'));
                            }
                        }
                        BX24.callMethod('entity.item.get', {ENTITY: "Fields"}, function (res) {
                            if (res.data() && res.data().length > 0)
                            {
                                var item = res.data()[0];
                                $("#tkp").val(item.PROPERTY_VALUES.tkp);
                            }
                        });
                    }
                });
            });

            $(document).ready(function () {
                $("#goBack").click(function () {
                    BX24.reloadWindow();
                });
                $("#save").click(function () {
                    var $btn = $(this).button('loading');
                    BX24.callMethod('entity.delete', {'ENTITY': 'Fields'}, function () {
                        BX24.callMethod('entity.add', {
                            'ENTITY': 'Fields',
                            'NAME': 'Fields',
                            'ACCESS': {AU: 'X'}
                        }, function () {
                            var batch = [];
                            batch.push(['entity.item.property.add', {
                                    ENTITY: 'Fields',
                                    PROPERTY: 'tkp',
                                    NAME: 'tkp',
                                    TYPE: 'S'}]);

                            BX24.callBatch(batch, function () {
                                BX24.callMethod('entity.item.add', {
                                    ENTITY: 'Fields',
                                    NAME: 'Fields',
                                    PROPERTY_VALUES: {
                                        tkp: $("#tkp").val()
                                    }
                                }, function (data) {
                                    $btn.button('reset');
                                });
                            });
                        });
                    });
                });
            });
        </script>
    </body>
</html>
