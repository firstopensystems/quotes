/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//    var logisticAggregates = $("#grid").date("kendoGrid").dataSource.aggregates().logistic;
function taxpricefunc(dataItem) {
    return Math.ceil(outpricefunc(dataItem));
}
function taxsumfunc(dataItem) {
    return dataItem.qty * taxpricefunc(dataItem);
}
function outpricefunc(dataItem) {
    return outsumfunc(dataItem) / dataItem.qty;
}
function outsumfunc(dataItem) {
    return supsumfunc(dataItem) + logisticfunc(dataItem) + margsumfunc(dataItem);
}
function suppricefunc(dataItem) {
    return dataItem.arm + dataItem.kof + dataItem.extra;
}
function supsumfunc(dataItem) {
    return suppricefunc(dataItem) * dataItem.qty;
}
function logisticfunc(dataItem) {
    var logisticAggregates = $("#grid").data("kendoGrid").dataSource.aggregates().logistic;
    var supsumAggregates = $("#grid").data("kendoGrid").dataSource.aggregates().supsum;
    return logisticAggregates.sum * supsumfunc(dataItem) / supsumAggregates.sum;
}
function margsumfunc(dataItem) {
    return supsumfunc(dataItem) * dataItem.marg / 100;
}
function profitsumfunc(dataItem) {
    return margsumfunc(dataItem) * 0.06;
}
function getFields(cb) {
    var finalFields = ["ID", "TITLE"];
    BX24.callMethod("crm.deal.fields", {}, function (result) {
        if (result.data()) {
            $.each(result.data(), function (index, item) {
                if (item.type === "file") {
                    finalFields.push(index);
                }
            });
            if (cb) {
                cb(finalFields);
            }
        }
    })
}